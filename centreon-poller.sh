#!/bin/bash
#This script is used to install a centreon poller in my personal server
#It may contain settings that are more adapted for my usage than general used
#Inspired by the documentation for centreon given here : https://www.sugarbug.fr

#exec &> /var/log/centreon-poller-install.log

#Need root to run this script

if [ "$EUID" -ne 0 ]
  then echo "This script needs to be run as root"
  exit
fi

IPCENTRAL='put here the IP of centreon central'
#IPPOLLER='Put here the IP of the poller to install' //Not needed actually
POLLERNAME='Place here the name of the server'

#Refresh packages index and install dependencies
apt install && apt upgrade -y
apt install cmake python3-pip -y

#Basedeps install for Debian10 install
apt install passwd git build-essential python-protobuf protobuf-compiler libprotobuf-dev curl -y




#Install Centreon-Clib
cd /usr/local/src
wget http://files.download.centreon.com/public/centreon-clib/centreon-clib-20.10.0.tar.gz
tar xzf centreon-clib-20.10.0.tar.gz 
cd centreon-clib-20.10.0
mkdir build
cd build
cmake \
       -DWITH_TESTING=0 \
       -DWITH_PREFIX=/usr  \
       -DWITH_SHARED_LIB=1 \
       -DWITH_STATIC_LIB=0 \
       -DWITH_PKGCONFIG_DIR=/usr/lib/pkgconfig ..

make && make install

##Centreon-connectors Installation
apt install libperl-dev libssh2-1-dev libgcrypt-dev -y
pip3 install conan
conan remote add centreon https://api.bintray.com/conan/centreon/centreon
cd /usr/local/src
wget http://files.download.centreon.com/public/centreon-connectors/centreon-connectors-20.10.0.tar.gz
tar xzf centreon-connectors-20.10.0.tar.gz 
cd centreon-connectors-20.10.0
mkdir build
cd build
conan install ..
cmake \
  -DWITH_PREFIX=/usr \
  -DWITH_PREFIX_BINARY=/usr/lib/centreon-connector  \
  -DWITH_CENTREON_CLIB_INCLUDE_DIR=/usr/include \
  -DWITH_TESTING=0 ..

make && make install

##Centreon Engine Install

#User creation
groupadd -g 6001 centreon-engine
useradd -u 6001 -g centreon-engine -m -r -d /var/lib/centreon-engine -c "Centreon-engine Admin" -s /bin/bash centreon-engine
#Install some dependencies
apt install libcgsi-gsoap-dev zlib1g-dev libssl-dev libxerces-c-dev -y

cd /usr/local/src
wget http://files.download.centreon.com/public/centreon-engine/centreon-engine-20.10.2.tar.gz
tar xzf centreon-engine-20.10.2.tar.gz 
cd centreon-engine-20.10.2
mkdir build
cd build
conan install ..
cmake  \
    -DWITH_CENTREON_CLIB_INCLUDE_DIR=/usr/include  \
    -DWITH_CENTREON_CLIB_LIBRARY_DIR=/usr/lib  \
    -DWITH_PREFIX=/usr  \
    -DWITH_PREFIX_BIN=/usr/sbin  \
    -DWITH_PREFIX_CONF=/etc/centreon-engine  \
    -DWITH_USER=centreon-engine  \
    -DWITH_GROUP=centreon-engine  \
    -DWITH_LOGROTATE_SCRIPT=1 \
    -DWITH_VAR_DIR=/var/log/centreon-engine  \
    -DWITH_RW_DIR=/var/lib/centreon-engine/rw  \
    -DWITH_STARTUP_SCRIPT=systemd  \
    -DWITH_STARTUP_DIR=/lib/systemd/system  \
    -DWITH_PKGCONFIG_SCRIPT=1 \
    -DWITH_SAMPLE_CONFIG=off  \
    -DWITH_PKGCONFIG_DIR=/usr/lib/pkgconfig  \
    -DWITH_TESTING=0  ..
make && make install
systemctl enable centengine.service && systemctl daemon-reload

##Plugins Installation + other dependencies
apt install monitoring-plugins 
apt install libgnutls28-dev libssl-dev libkrb5-dev libldap2-dev libsnmp-dev gawk libwrap0-dev libmcrypt-dev smbclient fping gettext dnsutils libmodule-build-perl libmodule-install-perl libnet-snmp-perl -y
#Centreon plugins deps
apt install libxml-libxml-perl libjson-perl libwww-perl libxml-xpath-perl libnet-telnet-perl libnet-ntp-perl libnet-dns-perl libnet-ldap-perl libdbi-perl libdbd-mysql-perl libdbd-pg-perl libdatetime-perl liburi-encode-perl libdate-manip-perl -y
#Build
cd /usr/local/src
wget http://files.download.centreon.com/public/centreon-plugins/centreon-plugins-20210218.tar.gz
tar xzf centreon-plugins-20210218.tar.gz 
cd centreon-plugins-20210218
cd /usr/local/src
wget http://files.download.centreon.com/public/centreon-plugins/centreon-plugins-20210218.tar.gz
tar xzf centreon-plugins-20210218.tar.gz 
cd centreon-plugins-20210218
chmod +x *
mkdir -p /usr/lib/centreon/plugins
mv * /usr/lib/centreon/plugins/

##Centreon Broker Installation
groupadd -g 6002 centreon-broker
useradd -u 6002 -g centreon-broker -m -r -d /var/lib/centreon-broker -c "Centreon-broker Admin"  -s /bin/bash centreon-broker
#Another lot of packages
apt-get install git librrd-dev libmariadb-dev libgnutls28-dev lsb-release liblua5.2-dev -y
#Usual compilation block
cd /usr/local/src
wget http://files.download.centreon.com/public/centreon-broker/centreon-broker-20.10.3.tar.gz
tar xzf centreon-broker-20.10.3.tar.gz
cd centreon-broker-20.10.3
mkdir build
cd build
conan install ..
cmake \
      -DWITH_DAEMONS='central-broker;central-rrd' \
      -DWITH_GROUP=centreon-broker \
      -DWITH_PREFIX=/usr  \
      -DWITH_PREFIX_BIN=/usr/sbin  \
      -DWITH_PREFIX_CONF=/etc/centreon-broker  \
      -DWITH_PREFIX_LIB=/usr/lib/centreon-broker \
      -DWITH_PREFIX_VAR=/var/lib/centreon-broker \
      -DWITH_PREFIX_MODULES=/usr/share/centreon/lib/centreon-broker \
      -DWITH_STARTUP_SCRIPT=systemd  \
      -DWITH_STARTUP_DIR=/lib/systemd/system  \
      -DWITH_TESTING=0 \
      -DWITH_USER=centreon-broker ..

make && make install

#SNMP

echo "Don't forget to configure the SNMP part"
echo "See snmp-conf-snippet for human memory refresh"

##Centreon Web Utilities
#Marrying users with groups
groupadd -g 6000 centreon
useradd -u 6000 -g centreon -m -r -d /var/lib/centreon -c "Centreon Admin" -s /bin/bash centreon
usermod -aG centreon-engine centreon
usermod -aG centreon-broker centreon
usermod -aG centreon centreon-engine
usermod -aG centreon centreon-broker
usermod -aG centreon-broker centreon-engine
#Installing apache n' shit because needed
apt install sudo apache2 ntp librrds-perl libconfig-inifiles-perl libnet-snmp-perl libdigest-hmac-perl libcrypt-des-ede3-perl libdbd-sqlite3-perl -y
#Installing Gorgone deps
apt install libzmq3-dev libssh-dev libextutils-makemaker-cpanfile-perl libmodule-build-perl libmodule-install-perl -y
#Compiling blocks again
cd /usr/local/src
wget http://search.cpan.org/CPAN/authors/id/M/MO/MOSCONI/ZMQ-LibZMQ4-0.01.tar.gz
tar zxf ZMQ-LibZMQ4-0.01.tar.gz && cd ZMQ-LibZMQ4-0.01
sed -i -e "s/tools/.\/tools/g" Makefile.PL
perl Makefile.PL
make && make install
cd ..
wget https://cpan.metacpan.org/authors/id/D/DM/DMAKI/ZMQ-Constants-1.04.tar.gz
tar zxf ZMQ-Constants-1.04.tar.gz && cd ZMQ-Constants-1.04
perl Makefile.PL
make && make install
cd /usr/local/src
git clone https://github.com/garnier-quentin/perl-libssh.git
cd perl-libssh
perl Makefile.PL
make
make install
#Other Perl deps
apt install libcryptx-perl libschedule-cron-perl libcrypt-cbc-perl libcpanel-json-xs-perl libjson-pp-perl libyaml-perl libyaml-libyaml-perl libdbd-sqlite3-perl libdbd-mysql-perl libapache-dbi-perl libdata-uuid-perl libhttp-daemon-perl libhttp-message-perl libmime-base64-urlsafe-perl libdigest-md5-file-perl libwww-curl-perl libio-socket-ssl-perl libnetaddr-ip-perl libhash-merge-perl -y
#Gorgone itself
cd /usr/local/src
wget http://files.download.centreon.com/public/centreon-gorgone/centreon-gorgone-20.10.2.tar.gz
tar xzf centreon-gorgone-20.10.2.tar.gz
cd centreon-gorgone-20.10.2
echo "In the next part, modify /etc/sysconfig by /etc/default"
stop 5
./install.sh -i
usermod -aG centreon-engine centreon-gorgone
usermod -aG centreon-broker centreon-gorgone
usermod -aG centreon centreon-gorgone
usermod -aG centreon-gorgone centreon
usermod -aG centreon-gorgone centreon-engine
usermod -aG centreon-gorgone centreon-broker

#Centreon Web Part
cd /usr/local/src
wget http://files.download.centreon.com/public/centreon/centreon-web-20.10.4.tar.gz
tar xzf centreon-web-20.10.4.tar.gz
cd centreon-web-20.10.4
echo "next step : yes or default for all, no webFront"
stop 5
./install.sh -i
#apache not needed anymore
apt remove apache2 -y

##Configuration
mkdir /var/log/centreon-broker
chown centreon-broker:centreon-broker  /var/log/centreon-broker
chmod 775 /var/log/centreon-broker
mkdir /var/cache/centreon
chown centreon:centreon  /var/cache/centreon
chmod 775 /var/cache/centreon
#adding sudo rule
tee /etc/sudoers.d/centreon <<EOF
## BEGIN: CENTREON SUDO

User_Alias      CENTREON=%centreon
Defaults:CENTREON !requiretty

# centreontrapd
CENTREON   ALL = NOPASSWD: /usr/sbin/service centreontrapd start
CENTREON   ALL = NOPASSWD: /usr/sbin/service centreontrapd stop
CENTREON   ALL = NOPASSWD: /usr/sbin/service centreontrapd restart
CENTREON   ALL = NOPASSWD: /usr/sbin/service centreontrapd reload

# Centreon Engine
CENTREON   ALL = NOPASSWD: /usr/sbin/service centengine start
CENTREON   ALL = NOPASSWD: /usr/sbin/service centengine stop
CENTREON   ALL = NOPASSWD: /usr/sbin/service centengine restart
CENTREON   ALL = NOPASSWD: /usr/sbin/service centengine reload
CENTREON   ALL = NOPASSWD: /usr/sbin/centengine -v *

# Centreon Broker
CENTREON   ALL = NOPASSWD: /usr/sbin/service cbd start
CENTREON   ALL = NOPASSWD: /usr/sbin/service cbd stop
CENTREON   ALL = NOPASSWD: /usr/sbin/service cbd restart
CENTREON   ALL = NOPASSWD: /usr/sbin/service cbd reload

## END: CENTREON SUDO
EOF
#Additional rights
chown centreon: /var/log/centreon
chmod 775 /var/log/centreon
chown centreon-broker: /etc/centreon-broker
chmod 775 /etc/centreon-broker
chmod -R 775 /etc/centreon-engine
chmod 775 /var/lib/centreon-broker
chown centreon: /etc/centreon-broker/*
chown centreon: /var/lib/centreon/centplugins
#Adding some additional conf
tee /lib/systemd/system/centreon.service <<EOF
[Unit]
Description=Cgroup Centreon

[Service]
Type=oneshot
ExecStart=/bin/true
ExecReload=/bin/true
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload && systemctl enable centreon

#Poller conf
cp /usr/local/src/centreon-web-20.10.4/bin/registerServerTopology* /usr/share/centreon/bin
chmod +x /usr/share/centreon/bin/registerServerTopology*
/usr/share/centreon/bin/registerServerTopology.sh -u admin -t Poller -h "$IPCENTRAL" -n "$POLLERNAME"
echo "Configure now the poller on central webUI \n Installation complete"